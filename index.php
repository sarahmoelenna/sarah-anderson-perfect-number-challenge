<?php
/**
* API to determine whether a given number is perfect, abundant, or deficient. This is based on the classification scheme for natural numbers by Nicomachus.
*/

require_once("api-response.php");
require_once("json-output.php");


$input =  isset($_GET['value']) ? $_GET['value'] : null;
$response = (new APIResponse($input))->buildResponse();
