<?php
/**
* APIResponse class will take a given input and is used to calculate the response of that input
*/
class APIResponse{

	private $input;

	/** 
	 * @param $input value for the api to use to calculate its classification
	 */
	public function __construct($input){
		$this->input = $input;
	}

	/**
	 *returns whether input is a whole number and equal to or above 0 or not
	 */
	protected function isInputValid(){
		return (is_numeric($this->input) && $this->input >= 0);
	}

	/**
	 * determines whether a given number is perfect, abundant, or deficient
	 * returns 'perfect', 'abundant', or 'deficient'
	 */
	protected function getClassification() {
		try{
			$factorArray = array_diff($this->getFactorsForValue($this->input), [$this->input]);
			$sumOfFactors = array_sum($factorArray);
			switch ($sumOfFactors) {
			    case $sumOfFactors == $this->input:
			        return "perfect";
			        break;
			    case $sumOfFactors > $this->input:
			        return "abundant";
			        break;
			    case $sumOfFactors < $this->input:
			        return "deficient";
			        break;
			    default:
			        return false;
			}
		} catch (Exception $e) {
			(new JSONOutput(["exception" => $e->getMessage()], 500))->outputJSON();
		}
	}

	/**
	 *Returns an array of factors for the given integer
	 * 
	 * @param value for the api to use to calculate its classification
	 */
	protected function getFactorsForValue($integer){
		$factors = [];
		try{
			for($i = 1; $i <= sqrt($integer); $i++){
			    if($integer % $i == 0){
			        $z = $integer/$i; 
			        array_push($factors, $i, $z);
			   	}
			}
		} catch (Exception $e) {
			(new JSONOutput(["exception" => $e->getMessage()], 500))->outputJSON();
		}
		return $factors;
	}

	/**
	 * processes what response the api will provide and generats the appropriate json output
	 */
	public function buildResponse(){
		if($this->isInputValid()){
			$result = $this->getClassification();
			if($result){
				(new JSONOutput(["value_provided" => $this->input, "classification" => $result], 200))->outputJSON();
			} else {
				(new JSONOutput(null, 400))->outputJSON();
			}
		} else {
			(new JSONOutput(null, 400))->outputJSON();
		}
	}

}