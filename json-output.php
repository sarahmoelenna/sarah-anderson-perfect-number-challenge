<?php
/**
* JSONOutput class will take a given array and status code to produce the finalised json output.
*/
class JSONOutput{

	private $dataArray;
	private $statusCode;

	/** 
	 * @param $arrayIn array to be inserted into the results elemnt of the json output
	 * @param $statusCode status code of the output
	 */
	public function __construct($arrayIn, $statusCode){
		$this->dataArray = is_array($arrayIn) ? $arrayIn : null;
		$this->statusCode = $statusCode;
	}

	/**
	* builds the json output, sets the headers and outputs the json
	*/
	public function outputJSON(){

		$finalArray = [];

		switch ($status = $this->statusCode) {
		    case $status == 200:
		        $finalArray['status_code'] = 200;
		        $finalArray['status_response'] = "OK";
		        break;
		    case $status == 400:
		        $finalArray['status_code'] = 400;
		        $finalArray['status_response'] = "Bad Request";
		        break;
		    case $status == 500:
		        $finalArray['status_code'] = 500;
		        $finalArray['status_response'] = "Internal Server Error";
		        break;
		}

		if(!is_null($this->dataArray)){
			$finalArray['results'] = $this->dataArray;
		}
		http_response_code($this->statusCode);
		header('Content-Type: application/json');
		echo(json_encode($finalArray));
		die();
	}
}