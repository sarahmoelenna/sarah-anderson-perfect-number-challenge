<?php

use PHPUnit\Framework\TestCase;
require_once("../api-response.php");

final class APIResponseTest extends APIResponse
{
    public function testPerfectNumberFactors()
    {
        $this->assertContains([1,2,3,6], APIResponse::getFactorsForValue(6));
    }

    public function testAbundantNumberFactors()
    {
        $this->assertContains([1,2,4,5,10], APIResponse::getFactorsForValue(20));
    }

    public function testDeficientNumberFactors()
    {
        $this->assertContains([1,2,4,8], APIResponse::getFactorsForValue(8));
    }

    public function testWholeIntegerIsValid()
    {
        $response = new APIResponse(6);
        $result = $response->isInputValid();
        $this->assertTrue($result);
    }

    public function testNullIsInvalid()
    {
        $response = new APIResponse(null);
        $result = $response->isInputValid();
        $this->assertFalse($result);
    }


	public function testStringIsInvalid()
    {
        $response = new APIResponse("string");
        $result = $response->isInputValid();
        $this->assertFalse($result);
    }

    public function testNegativeIsInvalid()
    {
        $response = new APIResponse(-6);
        $result = $response->isInputValid();
        $this->assertFalse($result);
    }

    public function testPerfectNumberClassification()
    {
        $response = new APIResponse(6);
        $result = $response->getClassification();
        $this->assertEquals($result, 'perfect');
    }

    public function testAbundantNumberClassification()
    {
        $response = new APIResponse(20);
        $result = $response->getClassification();
        $this->assertEquals($result, 'abundant');
    }

    public function testDeficientNumberClassification()
    {
        $response = new APIResponse(8);
        $result = $response->getClassification();
        $this->assertEquals($result, 'deficient');
    }

}

